##About

This is application illustrate a workers database.

##Install
Install for scratch:

First "composer install" to install laravel. 

1.First make sqlite database and place it in '/database' folder;
2.Change path parameter DB_DATABASE to your database in .env file;
3.Run command 'php artisan migrate' to create tables;
4.Run command 'php artisan db:seed' to create users;
5.Go to http://yourdomain/login and type email: test@test.ru and password: test1234
