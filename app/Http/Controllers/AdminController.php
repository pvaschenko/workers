<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\User;
use App\Position;
use App\Skill;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (Auth::check() && !Auth::user()->admin) {
            abort(403, 'Access denied');
        }
        return view('admin');
    }

    /**
     * Add position
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    public function position(Request $request)
    {
        $position = new Position();
        $position->name = $request->post('position');
        $position->save();

        return 'ok';
    }

    /**
     * Delete position
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    public function positionDelete(Request $request)
    {
        $position = Position::find($request->post('id'));
        $position->delete();

        return 'ok';
    }

    /**
     * Return all positions
     *
     * @return string
     */
    public function positions()
    {
        $response = [];
        $positions = Position::all()->toArray();
        $response['draw'] = 1;
        $response['recordsTotal'] = count($positions);
        $response['recordsFiltered'] = $response['recordsTotal'];
        foreach ($positions as $position) {
            $response['data'][] = ['name'=>$position['name'],'actions'=>$position['id']];
        }

        return json_encode($response);
    }

    /**
     * Add skill
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    public function skill(Request $request)
    {
        $skill = new Skill();
        $skill->name = $request->post('skill');
        $skill->save();

        return 'ok';
    }

    /**
     * Delete skill
     *
     * @param  \Illuminate\Http\Request $request
     * @return string
     */
    public function skillDelete(Request $request)
    {
        $skill = Skill::find($request->post('id'));
        $skill->delete();

        return 'ok';
    }

    /**
     * Return all skills
     *
     * @return string
     */
    public function skills()
    {
        $response = [];
        $skills = Skill::all()->toArray();
        $response['draw'] = 1;
        $response['recordsTotal'] = count($skills);
        $response['recordsFiltered'] = $response['recordsTotal'];
        foreach ($skills as $skill) {
            $response['data'][] = ['name'=>$skill['name'],'actions'=>$skill['id']];
        }

        return json_encode($response);
    }

    /**
     * Return all users
     *
     * @return string
     */
    public function users()
    {
        $response = [];
        $users = User::all()->toArray();
        $response['draw'] = 1;
        $response['recordsTotal'] = count($users);
        $response['recordsFiltered'] = $response['recordsTotal'];
        foreach ($users as $user) {
            if ($user['position']) {
                $position = Position::find($user['position'])->name;
            } else {
                $position = '';
            }
            if ($user['skills']) {
                $skills = json_decode($user['skills']);
                $skillName = '';
                if (is_array($skills)) {
                    foreach($skills as $skill) {
                        $skillName .= Skill::find($skill)->name.' ';
                    }
                } else {
                    if ($skills) {
                        $skillName = Skill::find($skills)->name;
                    }
                }
            } else {
                    $skillName = '';
            }
            if ($user['admin']) {
                $admin = 'Yes';
            } else {
                $admin = 'No';
            }
            $response['data'][] = [
                'surname' => $user['surname'],
                'name' => $user['name'],
                'middlename' => $user['middlename'],
                'phone' => $user['phone'],
                'email' => $user['email'],
                'position' => $position,
                'skills' => $skillName,
                'admin' => $admin,
                'registered' => date('d.m.Y H:i:s',strtotime($user['created_at'])),
                'actions' => $user['id']
            ];
        }

        return json_encode($response);
    }

    /**
     * Delete user
     *
     * @param  \Illuminate\Http\Request $request
     * @return string
     */
    public function userDelete(Request $request)
    {
        $user = User::find($request->post('id'));
        $user->delete();

        return 'ok';
    }

    /**
     * Edit user
     *
     * @param  \Illuminate\Http\Request $request
     * @return string
     */
    public function userEdit(Request $request)
    {
        $user = User::find($request->post('id'));

        return view('modal',['position'=>$user->position,
            'userSkills'=>json_decode($user->skills),
            'positions' => Position::all()->toArray(),
            'skills' => Skill::all()->toArray(),
            'admin' => $user['admin'],
            'userId' => $user['id']
        ]);
    }

    /**
     * Update user
     *
     * @param  \Illuminate\Http\Request $request
     * @return string
     */
    public function userUpdate(Request $request)
    {
        $user = User::find($request->post('id'));
        $user->position = $request->post('position');
        $user->skills = json_encode($request->post('skills'));
        $user->admin = $request->post('admin') ? 1 : 0;
        $user->save();

        return redirect()->route('admin');
    }


}
