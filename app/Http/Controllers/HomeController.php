<?php

namespace App\Http\Controllers;

use App\Position;
use App\Skill;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    /**
     * Return all users
     *
     * @return string
     */
    public function users()
    {
        $response = [];
        $users = User::all()->toArray();
        $response['draw'] = 1;
        $response['recordsTotal'] = count($users);
        $response['recordsFiltered'] = $response['recordsTotal'];
        foreach ($users as $user) {
            if ($user['position']) {
                $position = Position::find($user['position'])->name;
            } else {
                $position = '';
            }
            if ($user['skills']) {
                $skills = json_decode($user['skills']);
                $skillName = '';
                if (is_array($skills)) {
                    foreach($skills as $skill) {
                        $skillName .= Skill::find($skill)->name.' ';
                    }
                } else {
                    if ($skills) {
                        $skillName = Skill::find($skills)->name;
                    }
                }
            } else {
                $skillName = '';
            }

            $response['data'][] = [
                'surname' => $user['surname'],
                'name' => $user['name'],
                'middlename' => $user['middlename'],
                'email' => $user['email'],
                'phone' => $user['phone'],
                'position' => $position,
                'skills' => $skillName,
                'registered' => date('d.m.Y H:i:s',strtotime($user['created_at'])),
            ];
        }

        return json_encode($response);
    }
}
