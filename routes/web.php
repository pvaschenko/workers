<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes([
 'register' => true,
 'verify' => true,
 'reset' => true,
 'login' => true,
]);

Route::get('/','WelcomeController@index')->name('welcome');
Route::get('/users','WelcomeController@users')->name('welcomeUsers');

Route::group(['middleware' => 'auth'], function () {
    //Home
    Route::get('/home','HomeController@index')->name('home');
    Route::get('/home/users', 'HomeController@users')->name('homeUsers');
    //Admin
    Route::get('/admin','AdminController@index')->name('admin');
    //Positions
    Route::post('/admin/positionDelete', 'AdminController@positionDelete')->name('positionDelete');
    Route::post('/admin/position', 'AdminController@position')->name('position');
    Route::get('/admin/positions', 'AdminController@positions')->name('positions');
    //Skills
    Route::post('/admin/skillDelete', 'AdminController@skillDelete')->name('skillDelete');
    Route::post('/admin/skill', 'AdminController@skill')->name('skill');
    Route::get('/admin/skills', 'AdminController@skills')->name('skills');
    //Users
    Route::get('/admin/users', 'AdminController@users')->name('users');
    Route::post('/admin/userDelete', 'AdminController@userDelete')->name('userDelete');
    Route::post('/admin/userEdit', 'AdminController@userEdit')->name('userEdit');
    Route::post('/admin/userUpdate', 'AdminController@userUpdate')->name('userUpdate');

});
