<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserTest extends TestCase
{
    /**
     * Test user registration.
     *
     * @return void
     */
    public function testNewUserRegistration()
    {
        $this->visit('/register')
            ->type('TestName', 'name')
            ->type('TestSurname', 'surname')
            ->type('TestMiddle', 'middlename')
            ->type('TestEmail@email.test', 'email')
            ->type('+79999999999', 'phone')
            ->type('test1234', 'password')
            ->type('test1234', 'password_confirmation')
            ->press('Register')
            ->seePageIs('/home');
    }

    /**
     * Test user login.
     *
     * @return void
     */
    public function testUserLogin()
    {
        $this->visit('/login')
            ->type('TestEmail@email.test', 'email')
            ->type('test1234', 'password')
            ->press('Login')
            ->seePageIs('/home');
    }

    /**
     * Test user login.
     *
     * @return void
     */
    public function testUserAdmin()
    {
        $user = factory(\App\User::class,'admin')->create();

        $this->actingAs($user)
             ->visit('/admin');

        $this->actingAs($user)
             ->json('POST', '/admin/position',
                [
                        'position' => 'TestPosition',
                ])
            ->seeJson([
                          'ok' => true,
                      ]);

        $this->actingAs($user)
            ->json('POST', '/admin/skill',
                   [
                       'skill' => 'TestSkill',
                   ])
            ->seeJson([
                          'ok' => true,
                      ]);
    }
}
