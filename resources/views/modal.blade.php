<div class="modal-dialog">
    <div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" align="center"><b>{{__('Edit User')}}</b></h4>
    </div>
    <form method="POST" action="{{ route('userUpdate') }}">
        @csrf
        <input type="hidden" name="id" value="{{$userId}}">
        <div class="form-group row">
            <div class="col-md-6">
                <label for="position" class="col-md-4 col-form-label text-md-right">{{ __('Position') }}</label>
                <select class="form-select" name="position">
                    <option value="0">Select position</option>
                    @foreach ($positions as $pos)
                        <option value="{{$pos['id']}}" @if($pos['id'] == $position) selected @endif>{{$pos['name']}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-6">
                <label for="skills" class="col-md-4 col-form-label text-md-right">{{ __('Skills') }}</label>
                    @foreach ($skills as $skill)
                    <div class="form-check">
                        <input class="form-check-input" name="skills[]" type="checkbox" value="{{$skill['id']}}"
                           @if (is_array($userSkills))
                                @if(in_array($skill['id'],$userSkills)) checked @endif
                           @else
                               @if($skill['id'] == $userSkills) checked @endif
                           @endif
                        >
                        <label class="form-check-label" for="flexCheckChecked">
                            {{$skill['name']}}
                        </label>
                    </div>
                    @endforeach
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-6">
                <label for="skills" class="col-md-4 col-form-label text-md-right">{{ __('Administrator') }}</label>
                <div class="form-check">
                    <input class="form-check-input" name="admin" type="checkbox" value="1" @if($admin) checked @endif>
                    <label class="form-check-label" for="flexCheckChecked">
                        Yes
                    </label>
                </div>
            </div>
        </div>
        <div class="form-group row mb-0">
            <div class="col-md-8 offset-md-4">
                <button type="submit" id="update_user" class="btn btn-primary" data-id="{{$userId}}">
                    {{ __('Save') }}
                </button>
            </div>
        </div>
    </form>
    </div>
</div>
<script>
    $(document).ready( function () {
        $(document).on('click', 'update_user', function (e) {
            e.preventDefault();
            var data = {
                _token: $('input[name=_token]').val(),
                id: $(this).attr('data-id'),
                position: $('input[name=position]').val(),
                skills: $('input[name=skills]').val(),
                admin: $('input[name=admin]').val(),
            };

            $.ajax({
                url: '{{URL::to('/')}}' + '/admin/userUpdate',
                method: 'post',
                data: data,
                success: function (response, status, xhr, $form) {
                    if (response.error) {
                        toastr.error(response.error);
                    } else {
                        $('#edit-modal').modal('hide');
                    }
                }
            });
        });
    });
</script>
