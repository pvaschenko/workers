@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                        <form method="POST" action="{{ route('position') }}">
                            @csrf

                            <div class="form-group row">
                                <label for="position" class="col-md-4 col-form-label text-md-right">{{ __('Position') }}</label>

                                <div class="col-md-6">
                                    <input id="position" type="text" class="form-control @error('position') is-invalid
                                    @enderror" name="position" value="{{ old('position') }}" required autocomplete="position" autofocus>

                                    @error('position')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" id="add_position" class="btn btn-primary">
                                        {{ __('Add') }}
                                    </button>
                                </div>
                            </div>
                        </form>

                    <table id="positions" class="display">
                        <thead>
                        <tr>
                            <th>{{__('Name')}}</th>
                            <th>{{__('Actions')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>

                    <form method="POST" id="skillsForm" action="{{ route('skill') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="skill" class="col-md-4 col-form-label text-md-right">{{ __('Skill') }}</label>

                            <div class="col-md-6">
                                <input id="skill" type="text" class="form-control @error('skill') is-invalid
                                @enderror" name="skill" value="{{ old('skill') }}" required autocomplete="skill" autofocus>

                                @error('skill')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" id="add_skill" class="btn btn-primary">
                                    {{ __('Add') }}
                                </button>
                            </div>
                        </div>
                    </form>
                    <table id="skills" class="display">
                        <thead>
                        <tr>
                            <th>{{__('Name')}}</th>
                            <th>{{__('Actions')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                        <br>
                        <h1>{{__('Users')}}</h1>
                    <table id="users" class="display">
                        <thead>
                        <tr>
                            <th>{{__('Surname')}}</th>
                            <th>{{__('Name')}}</th>
                            <th>{{__('Middle name')}}</th>
                            <th>{{__('Email')}}</th>
                            <th>{{__('Phone')}}</th>
                            <th>{{__('Position')}}</th>
                            <th>{{__('Skills')}}</th>
                            <th>{{__('Admin')}}</th>
                            <th>{{__('Registered')}}</th>
                            <th>{{__('Actions')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="edit-modal"></div>
<script>
    $(document).ready( function () {

        var positions = new DataTable('#positions',{
            ajax: {
                url: '{{ route('positions') }}',
            },
            columns: [
                {data: 'name'},
                {data: 'actions', responsivePriority:-1},
            ],
            columnDefs:[
                { orderable: true, targets: [1] },
                {
                    targets:-1,
                    title:'{{__('Actions')}}',
                    orderable:false,
                    render:function(data,a,e,l){
                        return `
                            <button class="btn btn-primary btn--del" title="{{__('Delete')}}" data-id="${data}">{{__('Delete')}}</button>
                                `;
                    }
                }
            ],
        });

        $(document).on('click','.btn--del',function (e){
            e.preventDefault();
            var data = {
                _token: $('input[name=_token]').val(),
                id: $(this).attr('data-id')
            };

            $.ajax({
                url: '{{URL::to('/')}}' + '/admin/positionDelete',
                method: 'post',
                data: data,
                success: function (response, status, xhr, $form) {
                    if (response.error) {
                        toastr.error(response.error);
                    } else {
                        positions.ajax.reload();
                    }
                }
            });
        });

        $('#add_position').click(
            function (e) {
                e.preventDefault();
                $.ajax({
                    method: "POST",
                    url: '{{ route('position') }}',
                    data: {'_token': $('[name="_token"]').val(), 'position': $('#position').val()},
                    success: function (response, status, xhr, $form) {
                        if (response.error) {
                            toastr.error(response.error);
                        } else {
                            positions.ajax.reload();
                        }
                    }
                });
            }
        );

        var skills = new DataTable('#skills',{
            ajax: {
                url: '{{ route('skills') }}',
            },
            columns: [
                {data: 'name'},
                {data: 'actions', responsivePriority:-1},
            ],
            columnDefs:[
                { orderable: true, targets: [1] },
                {
                    targets:-1,
                    title:'{{__('Actions')}}',
                    orderable:false,
                    render:function(data,a,e,l){
                        return `
                            <button class="btn btn-primary btn--del-skill" title="{{__('Delete')}}" data-id="${data}">{{__('Delete')}}</button>
                                `;
                    }
                }
            ],
        });

        $(document).on('click','.btn--del-skill',function (e){
            e.preventDefault();
            var data = {
                _token: $('input[name=_token]').val(),
                id: $(this).attr('data-id')
            };

            $.ajax({
                url: '{{URL::to('/')}}' + '/admin/skillDelete',
                method: 'post',
                data: data,
                success: function (response, status, xhr, $form) {
                    if (response.error) {
                        toastr.error(response.error);
                    } else {
                        skills.ajax.reload();
                    }
                }
            });
        });

        $('#add_skill').click(
            function (e) {
                e.preventDefault();
                $.ajax({
                    method: "POST",
                    url: '{{ route('skill') }}',
                    data: {'_token': $('[name="_token"]').val(), 'skill': $('#skill').val()},
                    success: function (response, status, xhr, $form) {
                        if (response.error) {
                            toastr.error(response.error);
                        } else {
                            skills.ajax.reload();
                        }
                    }
                });
            }
        );

        var users = new DataTable('#users',{
            ajax: {
                url: '{{ route('users') }}',
            },
            columns: [
                {data: 'surname'},
                {data: 'name'},
                {data: 'middlename'},
                {data: 'email'},
                {data: 'phone'},
                {data: 'position'},
                {data: 'skills'},
                {data: 'admin'},
                {data: 'registered'},
                {data: 'actions', responsivePriority:-1},
            ],
            columnDefs:[
                { orderable: true, targets: [1] },
                {
                    targets:-1,
                    title:'{{__('Actions')}}',
                    orderable:false,
                    render:function(data,a,e,l){
                        return `
                            <button class="btn btn-primary btn--del-user" title="{{__('Delete')}}" data-id="${data}">{{__('Delete')}}</button>
                            <button class="btn btn-primary btn--edit-user" title="{{__('Edit')}}" data-id="${data}">{{__('Edit')}}</button>
                                `;
                    }
                }
            ],
        });

        $(document).on('click','.btn--del-user',function (e){
            e.preventDefault();
            var data = {
                _token: $('input[name=_token]').val(),
                id: $(this).attr('data-id')
            };

            $.ajax({
                url: '{{URL::to('/')}}' + '/admin/userDelete',
                method: 'post',
                data: data,
                success: function (response, status, xhr, $form) {
                    if (response.error) {
                        toastr.error(response.error);
                    } else {
                        users.ajax.reload();
                    }
                }
            });
        });

        $(document).on('click','.btn--edit-user',function (e){
            e.preventDefault();
            var data = {
                _token: $('input[name=_token]').val(),
                id: $(this).attr('data-id')
            };

            $.ajax({
                url: '{{URL::to('/')}}' + '/admin/userEdit',
                method: 'post',
                data: data,
                success: function (response, status, xhr, $form) {
                    if (response.error) {
                        toastr.error(response.error);
                    } else {
                        $('#edit-modal').html(response);
                        $('#edit-modal').modal();
                    }
                }
            });
        });
    });

</script>
@endsection
