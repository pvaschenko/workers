@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <h1>{{__('Users')}}</h1>
                    <table id="users" class="display">
                        <thead>
                        <tr>
                            <th>{{__('Surname')}}</th>
                            <th>{{__('Name')}}</th>
                            <th>{{__('Middle name')}}</th>
                            <th>{{__('Email')}}</th>
                            <th>{{__('Phone')}}</th>
                            <th>{{__('Position')}}</th>
                            <th>{{__('Skills')}}</th>
                            <th>{{__('Registered')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
    <script>
        $(document).ready( function () {
            var users = new DataTable('#users', {
                ajax: {
                    url: '{{ route('homeUsers') }}',
                },
                columns: [
                    {data: 'surname'},
                    {data: 'name'},
                    {data: 'middlename'},
                    {data: 'email'},
                    {data: 'phone'},
                    {data: 'position'},
                    {data: 'skills'},
                    {data: 'registered'},
                ],
            });
        });
    </script>
@endsection
